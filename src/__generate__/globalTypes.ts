/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum DayOfWeek {
  FRIDAY = "FRIDAY",
  MONDAY = "MONDAY",
  SATURDAY = "SATURDAY",
  SUNDAY = "SUNDAY",
  THURSDAY = "THURSDAY",
  TUESDAY = "TUESDAY",
  WEDNESDAY = "WEDNESDAY",
}

export enum OrderClass {
  TakeAway = "TakeAway",
  ToTable = "ToTable",
}

export enum OrderType {
  Delivery = "Delivery",
  PickUp = "PickUp",
  TakeAway = "TakeAway",
}

export interface RestaurantFilter {
  type: OrderClass;
  tagIds: string[];
}

//==============================================================
// END Enums and Input Objects
//==============================================================
