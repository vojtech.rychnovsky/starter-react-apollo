const colors = {
  // main colors
  primary: '#fff',
  secondary: '#30323C',

  // theme colors
  alpha: '#F4282D',

  // state colors
  success: '#37d773',
  error: '#F4282D',
}

export default colors
