export enum LocalStorageKeys {
  USER_TOKEN = 'user_token',
}

export const removeLocalStorage = (key: string) => {
  localStorage.removeItem(key)
}

export const setLocalStorage = (key: string, value: string) => {
  localStorage.setItem(key, value)
}

export const getLocalStorage = (key: LocalStorageKeys) => {
  return localStorage.getItem(key) || ''
}
