import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import routes from '../constants/routes'

// pages
import MainPage from '../pages/MainPage'

const App: React.FC = () => {
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route exact={true} path={routes.index} component={MainPage} />
          <Route component={NoMatch} />
        </Switch>
      </Router>
    </React.Fragment>
  )
}

// 404 route
const NoMatch: React.FC = () => (
  <Redirect exact={true} to={{ pathname: routes.index }} />
)

export default App
