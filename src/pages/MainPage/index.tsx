import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectRestaurants } from '../../data/Restaurant/selectors'
import { handleGetRestaurants } from '../../data/Restaurant/thunks'

import { H1 } from '../../constants/typography'

const MainPage = () => {
  const restaurants = useSelector(selectRestaurants)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(handleGetRestaurants())
  }, [dispatch])

  return (
    <React.Fragment>
      <H1>Hey, welcome to Pingl!</H1>
      <p>Choose restaurant, where you want to eat:</p>
      <ul>
        {restaurants.map(restaurant => (
          <li key={restaurant.id}>{restaurant.name}</li>
        ))}
      </ul>
    </React.Fragment>
  )
}

export default MainPage
