import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient, DefaultOptions } from 'apollo-client'
// tslint:disable-next-line:no-implicit-dependencies
import { ApolloLink, split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
// tslint:disable-next-line:no-implicit-dependencies
import { getMainDefinition } from 'apollo-utilities'
import { getLocalStorage, LocalStorageKeys } from './utils/localStorage'

import config from './config'
import { getCurrentLanguageWithoutLocale } from './i18n'

const cache = new InMemoryCache()

// Create an http link:
const httpLink = new HttpLink({
  uri: config.REACT_APP_HOST_URL,
})

// Create a web socket link:
const wsLink = new WebSocketLink({
  uri: config.REACT_APP_HOST_URL_WS,
  options: {
    reconnect: true,
    timeout: 300000, // Keep alive for 5 minutes
    lazy: true,
  },
})

// Split request to http or ws link based on request type
const combinedLink = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  },
  wsLink,
  httpLink
)

// TODO: implement refetching, error handling, refresh tokens...

// Middleware to add JWT token to headers
const authMiddleware = new ApolloLink((operation, forward) => {
  const token: string = getLocalStorage(LocalStorageKeys.USER_TOKEN)
  const authHeader = token ? `Bearer ${token}` : ''
  operation.setContext(({ headers = {} }: Record<string, any>) => ({
    headers: {
      ...headers,
      'Accept-Languages': getCurrentLanguageWithoutLocale(),
      Authorization: authHeader,
    },
  }))
  return forward(operation)
})

// Options for the ApolloClient
const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    errorPolicy: 'all',
  },
}

// Combine all together to Apollo client
const client = new ApolloClient({
  cache,
  link: combinedLink.concat(authMiddleware),
  defaultOptions,
})

export default client
