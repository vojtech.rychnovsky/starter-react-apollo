import { GET_RESTAURANTS } from './gql/queries'
import { gqlApi } from '../gqlApi'
import {
  getRestaurants,
  getRestaurantsVariables,
} from './gql/__generated__/getRestaurants'

/* QUERIES */
export const requestGetRestaurants = async (
  variables: getRestaurantsVariables
) => {
  return (
    await gqlApi.apiQueryRequest<getRestaurants, getRestaurantsVariables>(
      GET_RESTAURANTS,
      variables
    )
  ).getRestaurants
}
