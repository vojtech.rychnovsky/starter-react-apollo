import { TAction } from '../store'
import {
  loadRestaurantsRequest,
  loadRestaurantsSuccess,
  loadRestaurantsFailure,
} from './actions'
import * as restaurantApi from './api'
import { IRestaurantsFilter } from './gql/types'
import { OrderClass } from '../../__generate__/globalTypes'

export const handleGetRestaurants: TAction<void> = () => async dispatch => {
  dispatch(loadRestaurantsRequest())
  // TODO: implement better filtering
  const filter: IRestaurantsFilter = {
    tagIds: [],
    type: OrderClass.TakeAway,
  }
  try {
    const result = await restaurantApi.requestGetRestaurants({ filter })
    dispatch(loadRestaurantsSuccess(result))
  } catch (e) {
    dispatch(loadRestaurantsFailure())
  }
}
