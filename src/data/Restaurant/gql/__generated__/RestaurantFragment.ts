/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { OrderType, DayOfWeek } from "./../../../../__generate__/globalTypes";

// ====================================================
// GraphQL fragment: RestaurantFragment
// ====================================================

export interface RestaurantFragment_info_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface RestaurantFragment_info {
  __typename: "RestaurantInfo";
  phone: string;
  website: string | null;
  websiteUrl: string | null;
  instagramUrl: string | null;
  facebookUrl: string | null;
  coverPhotoUrl: string;
  placeAddress: string;
  tags: RestaurantFragment_info_tags[];
}

export interface RestaurantFragment_tables {
  __typename: "Table";
  id: string;
  name: string;
  orderType: OrderType;
}

export interface RestaurantFragment_config_languages {
  __typename: "Language";
  locale: string;
}

export interface RestaurantFragment_config_customer {
  __typename: "CustomerConfig";
  tipsEnabled: boolean;
}

export interface RestaurantFragment_config_openTimes_time_open {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface RestaurantFragment_config_openTimes_time_close {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface RestaurantFragment_config_openTimes_time {
  __typename: "TimeInterval";
  open: RestaurantFragment_config_openTimes_time_open;
  close: RestaurantFragment_config_openTimes_time_close;
}

export interface RestaurantFragment_config_openTimes {
  __typename: "ActiveTime";
  day: DayOfWeek;
  time: RestaurantFragment_config_openTimes_time;
}

export interface RestaurantFragment_config {
  __typename: "RestaurantConfig";
  languages: RestaurantFragment_config_languages[];
  customer: RestaurantFragment_config_customer;
  openTimes: RestaurantFragment_config_openTimes[];
  isOpen: boolean;
  isActive: boolean;
  orderTypes: OrderType[];
}

export interface RestaurantFragment {
  __typename: "RestaurantPublic";
  id: string;
  name: string;
  info: RestaurantFragment_info;
  tables: RestaurantFragment_tables[];
  config: RestaurantFragment_config;
}
