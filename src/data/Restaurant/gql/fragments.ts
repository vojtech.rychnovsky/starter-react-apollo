import gql from 'graphql-tag'

export const RESTAURANT_FRAGMENT = gql`
  fragment RestaurantFragment on RestaurantPublic {
    id
    name
    info {
      phone
      website
      websiteUrl
      instagramUrl
      facebookUrl
      coverPhotoUrl
      placeAddress
      tags {
        id
        name
      }
    }
    tables {
      id
      name
      orderType
    }
    config {
      languages {
        locale
      }
      customer {
        tipsEnabled
      }
      openTimes {
        day
        time {
          open {
            hour
            minute
          }
          close {
            hour
            minute
          }
        }
      }
      isOpen
      isActive
      orderTypes
    }
  }
`
