import gql from 'graphql-tag'
import { RESTAURANT_FRAGMENT } from './fragments'

export const GET_RESTAURANTS = gql`
  query getRestaurants($filter: RestaurantFilter!) {
    getRestaurants(filter: $filter) {
      ...RestaurantFragment
    }
  }
  ${RESTAURANT_FRAGMENT}
`
