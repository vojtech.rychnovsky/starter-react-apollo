import { RestaurantFragment } from './__generated__/RestaurantFragment'
import { RestaurantFilter } from '../../../__generate__/globalTypes'

export type IRestaurant = RestaurantFragment

export type IRestaurantsFilter = RestaurantFilter
