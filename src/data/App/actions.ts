import { action } from 'typesafe-actions'
import { EXAMPLE_CONST } from './constants'

export const exampleAction = () => action(EXAMPLE_CONST)
