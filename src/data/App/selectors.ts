import { IReduxState } from '../store'

export const selectAppState = (state: IReduxState) => state.app
