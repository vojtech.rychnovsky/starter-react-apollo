import reducer from '../reducers'

describe('App reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {} as any)).toEqual({})
  })
})
