import { DocumentNode } from 'graphql'
import client from '../client'

// Generic API for remote graphql requests

// QUERY
const apiQueryRequest = async <Response, Variables = {}>(
  query: DocumentNode,
  variables: Variables
) => {
  console.log(variables)
  const result = await client.query<Response, Variables>({
    query,
    variables,
  })

  if (result.errors) {
    throw Error(result.errors[0].message)
  }

  return result.data
}

// MUTATION
const apiMutateRequest = async <Response, Variables = {}>(
  mutation: DocumentNode,
  variables?: Variables
) => {
  const result = await client.mutate<Response, Variables>({
    mutation,
    variables,
  })

  if (result.errors) {
    throw Error(result.errors[0].message)
  }

  return result.data
}

// SUBSCRIPTION
const apiSubscribeRequest = async <Response, Variables = {}>(
  query: DocumentNode,
  variables?: Variables
) => {
  return client.subscribe<Response, Variables>({
    query,
    variables,
  })
}

// export as one API object
export const gqlApi = {
  apiQueryRequest,
  apiMutateRequest,
  apiSubscribeRequest,
}
