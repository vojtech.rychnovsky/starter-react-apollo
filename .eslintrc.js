module.exports = {
  extends: ['react-app', 'prettier', 'prettier/react'],
  plugins: ['react-hooks'],
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      modules: true,
    },
  },
}
